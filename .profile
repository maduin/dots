#!/bin/sh

# Profile: Runs on login.

export PATH="$PATH:$HOME/.local/scripts"
export EDITOR="vim"

# Home Clean up
export HISTFILE="$HOME/.cache/shell_history"
export __GL_SHADER_DISK_CACHE_PATH="$HOME/.cache/nvidia"
export LESSHISTFILE="-"

# Source .bashrc
[ -f ~/.bashrc ] && . ~/.bashrc
